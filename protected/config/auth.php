<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
	'guest' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule' => null,
		'data' => null
	),
	'1' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'User',
		'children' => array(
			'guest', // унаследуемся от гостя
		),
		'bizRule' => null,
		'data' => null
	),
	'2' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Admin',
		'children' => array(
			'user',          // позволим модератору всё, что позволено пользователю
		),
		'bizRule' => null,
		'data' => null
	),
);