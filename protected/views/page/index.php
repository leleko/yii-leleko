<?php
/* @var $this PageController */

$this->breadcrumbs=array(
	'Категория: '.$category->title,
);
?>
<?php
foreach ($models as $one) {
    //echo '<h3>',$one->title,'</h3>';
    echo CHtml::link('<h3>'.$one->title.'</h3>',array('view', 'id'=> $one->id));
    echo substr($one->content,0,260);
    echo CHtml::link('Читать далее...',array('view', 'id'=> $one->id));
    echo "<hr>";
}
if (!$models) {
    echo 'В данной категории статей нет.';
}
?>