<?php
/* @var $this PageController */

$this->breadcrumbs=array(
	'Категория: '.$model->category->title => array('index','id' => $model->category_id),
    $model->title,
);
?>
<?php
    echo '<h1>'.$model->title.'</h1>';

?>
<?php
    echo date('j.m.Y H:i',$model->created);
        echo '<hr>';
?>
<?php
    echo $model->content;
?>

<?php
/* @var $this CommentController */
/* @var $model Comment */
/* @var $form CActiveForm */
?>
<hr />
<?php if(Yii::app()->user->hasFlash('comment')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('comment'); ?>
</div>

<?php else: ?>
<?php
    echo $this->renderPartial('newComment', array('model' => $newComment));
?>
<?php endif; ?>
<h1>Комментарии</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=> Comment::all($model->id),
    'itemView'=>'_viewComment',
)); ?>