<?php
/* @var $this CategoryController */
/* @var $model Category */

//$this->breadcrumbs=array(
//	'Категории'=>array('index'),
//	'Создание',
//);

$this->menu=array(
	array('label'=>'Журнал категорий', 'url'=>array('index')),
	/*array('label'=>'Manage Category', 'url'=>array('admin')),*/
);
?>

<h1>Создание новой категории</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>