<?php
/* @var $this CategoryController */
/* @var $model Category */
//
//$this->breadcrumbs=array(
//	'Categories'=>array('index'),
//	$model->title=>array('view','id'=>$model->id),
//	'Update',
//);

$this->menu=array(
	array('label'=>'Журнал категорий', 'url'=>array('index')),
	array('label'=>'CСоздании категории', 'url'=>array('create')),
);
?>

<h1>Изменение категории <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>